#include <stdio.h>
#include <xxhash.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>
#include <byteswap.h>

unsigned long long calc_hash(const void* buffer, size_t length)
{
    unsigned long long const seed = 0;   /* or any other value */
    unsigned long long const hash = XXH64(buffer, length, seed);
    return hash;
}

/* https://techoverflow.net/2013/08/21/a-simple-mmap-readonly-example/ 
*/
size_t getFilesize(const char* filename) 
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

#define Testschluessel "c_testschluessel.bin"
#define BFFILE "bloomfilter-xxhash64-77377"
#define BF_p 77377

int main(int argc, char** argv) 
{
    clock_t zeit;
    int offset, bf_entry;
    unsigned long long current_hashvalue, my_byteorderswapped;

    size_t filesize = getFilesize(Testschluessel);
    int fd = open(Testschluessel, O_RDONLY, 0); assert(fd != -1);
    const char * mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0); assert(mmappedData != MAP_FAILED);

    size_t bf_filesize = getFilesize(BFFILE);
    int bf_fd = open(BFFILE, O_RDONLY, 0); assert(bf_fd != -1);
    const char * BF = mmap(NULL, bf_filesize, PROT_READ, MAP_PRIVATE, bf_fd, 0); assert(mmappedData != MAP_FAILED);

    unsigned long int Elemente=filesize>>8;
    printf("Testdateigröße: %ld, Anzahl RSA-Moduli: %ld\n", filesize, Elemente);
    printf("Größe Bloomfilter: %ld, Anzahl Bit-Zellen: %ld\n", bf_filesize, bf_filesize/4/8 );

    printf("Los geht's\n");
    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();
    for (unsigned long int i=0; i<Elemente; i++) 
    {
        current_hashvalue=calc_hash(&mmappedData[i<<8], 256);
        /* Ich habe den Bloomfilter (bloomfilter-xxhash64-77377) aus der Perspektive C 
         * in der falschen Byteorder abgelegt.
         * https://stackoverflow.com/questions/2182002/convert-big-endian-to-little-endian-in-c-without-using-provided-func
         * Für die python-Implementierung eher unrelevant. 
         * Wenn man nur C im Blick hat sollte man wahrscheinlich den Bloomfilter mit "richtiger"
         * Byteorder neu erzeugen um sich "swapping" zu sparen (kostet unnötig Rechenzeit).
        */
        my_byteorderswapped=bswap_64(current_hashvalue);
        // printf("Hi %ld\n", current_hashvalue);
        bf_entry= current_hashvalue % BF_p;
        // printf("H2 %ld\n", bf_entry);
        // printf("Element: %d, offset1: %d, bf-element-nr: %d\n", i, i<<8, bf_entry);
        for (int a=0; a<4; a++) 
        {
            int bf_real_offset=bf_entry*4*8 + a*8;

            /*
            for (int x=0; x<8; x++) {
                printf("H3 %d\n", (unsigned char) BF[bf_real_offset+x]);
            }
            unsigned long long * myp;
            unsigned long long test2=bswap_64(current_hashvalue);
            printf("H4  %ld\n", (unsigned long long) BF[bf_real_offset]);
            myp=(unsigned long long *) &BF[bf_real_offset];
            printf("H5  %ld\n", (unsigned long long) *myp);
            printf("H6  %ld\n", current_hashvalue);
            printf("H7  %ld\n", test2);
            exit(1);
            */

            if (
                * (unsigned long long *) &BF[bf_real_offset] == my_byteorderswapped
               )
            {
                printf("Gefunden! Schwacher Schlüssel bei Index-Nummer: %d\n", i);
            }
        }
    }
    zeit=clock() - zeit;
    double zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("Fertig\n");

    printf("Benötigte Zeit: %.9f Sekunden, Zeit (Sekunden) pro Element: %.9f\n", zeit_diff, zeit_diff / ((double) Elemente));
    
    close(fd); close(bf_fd);

    printf("bye\n");

    return 0;
}

