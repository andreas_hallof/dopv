LIBPATH=/usr/local/lib

c_dopv-test-via-bloomfilter: c_dopv-test-via-bloomfilter.c
	gcc -O3 -o $@ $< -lxxhash -L$(LIBPATH)

.PHONY: c_testschluessel.bin

c_testschluessel.bin:
	dd if=/dev/urandom bs=256 of=$@ count=1000000

