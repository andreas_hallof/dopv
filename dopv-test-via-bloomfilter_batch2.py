#! /usr/bin/env python3

import sys, hashlib, os, xxhash, mmap
from pathlib import Path
from time import time

p=77377
BFFILE="bloomfilter-xxhash64-77377"
TESTSCHLÜSSEL="c_testschluessel.bin"

if not os.path.isfile(BFFILE):
    sys.exit("Ich kann meinen Bloomfilter '{}' nicht finden. -> exit".format(BFFILE))
if not os.path.isfile(TESTSCHLÜSSEL):
    sys.exit("Ich kann meine Testschlüssel '{}' nicht finden. -> exit".format(TESTSCHLÜSSEL))

weak_keys=0
with open(TESTSCHLÜSSEL, 'rb') as testschlüssel:
    testschlüsselfilesize=os.stat(testschlüssel.fileno()).st_size 
    assert testschlüsselfilesize>=256;
    Schlüsselanzahl=testschlüsselfilesize>>8
    assert Schlüsselanzahl<<8==testschlüsselfilesize

    print("Ich untersuche {} RSA-Moduli".format(Schlüsselanzahl))

    with open(BFFILE, 'r') as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as m:
            print("Los geht's")
            startzeit=time()
            for i_schlüssel in range(0, Schlüsselanzahl):
                #print("Schlüssel {}".format(i_schlüssel))
                data=testschlüssel.read(256)
                h=xxhash.xxh64(data).digest()
                print(int.from_bytes(h, byteorder='big', signed=False))
                hi=xxhash.xxh64(data).intdigest()
                print("intdigest=",hi)
                elementnr=int.from_bytes(h, byteorder='big', signed=False) % p
                print("Elementnr", elementnr)
                offset=elementnr*4*8
                for i in [0, 1, 2, 3]:
                    print(i, m[offset+i*8:offset+i*8+8], h)
                    sys.exit(1)
                    if m[offset+i*8:offset+i*8+8]==h:
                       print("Nr. {}: Schwacher Schlüssel (Debian Openssl PRNG Vulnerability).".format(
                                i_schlüssel)
                       )
                       weak_keys+=1

            zeitdiff=time()-startzeit
            print("Fertig")
            print("Benötigte Zeit: {} Sekunden, Zeit (Sekunden) pro Element: {}".format(
                    zeitdiff, zeitdiff / float(Schlüsselanzahl)))

            if weak_keys>0:
                print(weak_keys,"schwache Schlüssel gefunden")

