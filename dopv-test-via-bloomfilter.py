#! /usr/bin/env python3

import sys, hashlib, os, xxhash, mmap
from pathlib import Path

p=77377
BFFILE="bloomfilter-xxhash64-77377"

if not os.path.isfile(BFFILE):
    sys.exit("Ich kann meinen Bloomfilter '{}' nicht finden. -> exit".format(BFFILE))

if len(sys.argv)<2:
    sys.exit("Ich erwarte einen Dateinamen als erstes Argument mit dem RSA-Moduls als Zahl in Hexadezimaldarstellung")

my_file=Path(sys.argv[1]);
if my_file.exists() and my_file.is_file():
    tmp=my_file.read_text();
    modulus=int(tmp,16); #print("modulus=0x{0:x}".format(modulus))
else:
    sys.exit('Kann Datei "{}" nicht lesen.'.format(sys.argv[1]))

binaer=modulus.to_bytes(256, byteorder = 'big')
h=xxhash.xxh64(binaer).digest()

elementnr=int.from_bytes(h, byteorder='big', signed=False) % p
offset=elementnr*4*8

with open(BFFILE, 'r') as f:
    with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as m:
        for i in [0, 1, 2, 3]:
            if m[offset+i*8:offset+i*8+8]==h:
                sys.exit("Schwacher Schlüssel (Debian Openssl PRNG Vulnerability).")

