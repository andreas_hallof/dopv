#! /usr/bin/env python3

import sys, hashlib, os, xxhash

# mir reicht hier diese "Kinder-Variante" für den Primzahltest
# Modul gmpy2 mit gmpy2.next_prime(2**16+1) und gmpy2.is_prime() ist besser
#
def isPrime(Number):
   return 2 in [Number,2**Number%Number]

p=77377
assert isPrime(p)

print("Bloomfiltersize: {} B ({} KiB)".format( p*4*8, p*4*8 >> 10))
BF=bytearray(p*4*8)
print(len(BF))

#sys.exit(0)

moduluslistfile=open("moduluslistfile.txt","rt");
counter=0
nullvektor=b'\x00\x00\x00\x00\x00\x00\x00\x00'
assert len(nullvektor)==8

for x in moduluslistfile:
   counter+=1;
   n=int(x,16)
   binaer=n.to_bytes(256,byteorder = 'big')
   h=xxhash.xxh64(binaer).digest()

   elementnr=int.from_bytes(h, byteorder='big', signed=False) % p
   offset=elementnr*4*8
   #print("c e o",counter, elementnr, offset)
   i=0
   while True:
      if BF[offset+i*8:offset+i*8+8]==nullvektor:
        BF[offset+i*8:offset+i*8+8]=h
        break 
      else:
        i+=1
        assert i<4

moduluslistfile.close()

with open("bloomfilter-xxhash64-"+str(p),"wb") as f:
    f.write(BF)

