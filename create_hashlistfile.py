#! /usr/bin/env python3

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

import math, sys, hashlib, os

hashlistfile=open("hashlistfile.txt","wt");
hashlist=[]
counter=0

for a in os.listdir("rsa/2048"): 
    a=os.path.join("rsa/2048",a)
    if os.path.isfile(a) and not a.endswith(".pub"):
        with open(a,"rb") as f:
            counter+=1
            private_key = serialization.load_pem_private_key(
                            f.read(), password=None, backend=default_backend()
                          )
            public_key = private_key.public_key()
            n=public_key.public_numbers().n
            h=hashlib.sha256(n.to_bytes(256,byteorder = 'big')).hexdigest()
            print(counter, h)

            hashlist.append(h)

print("Schreibe Hashliste mit", len(hashlist), "Einträgen.")
for x in sorted(hashlist):
    hashlistfile.write(x+"\n")
hashlistfile.close()

