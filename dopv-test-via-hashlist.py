#! /usr/bin/env python3

import sys, hashlib, os
from pathlib import Path

HASHLISTFILE="hashlistfile.txt"

if not os.path.isfile(HASHLISTFILE):
    sys.exit("Ich kann meine Hashtabelle '{}' nicht finden. -> exit".format(HASHLISTFILE))

if len(sys.argv)<2:
    sys.exit("Ich erwarte einen Dateinamen als erstes Argument mit dem RSA-Moduls als Zahl in Hexadezimaldarstellung.")

my_file=Path(sys.argv[1]);
if my_file.exists() and my_file.is_file():
    tmp=my_file.read_text();
    modulus=int(tmp,16)
    #print("modulus=0x{0:x}".format(modulus))
else:
    sys.exit('Kann Datei "{}" nicht lesen.'.format(sys.argv[1]))

zielhashwert=hashlib.sha256(modulus.to_bytes(256,byteorder = 'big')).hexdigest()

with open(HASHLISTFILE, "rt") as f:
    for x in f:
        x=x.rstrip()
        if x==zielhashwert:
            sys.exit("Schwacher Schlüssel (Debian Openssl PRNG Vulnerability).")

