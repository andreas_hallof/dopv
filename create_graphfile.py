#! /usr/bin/env python3

import networkx as nx
from networkx.readwrite import json_graph
import json, math, sys, hashlib, os

import matplotlib.pyplot as plt


g=nx.DiGraph(); Startsymbol=g.add_node("Start")

hashlistfile=open("hashlistfile.txt","rt");
counter=0

for h in hashlistfile:
   h=h.rstrip(); counter+=1; print(counter, h)
   node="Start"; t=""
   for x in h:
      t=t+x
      if t in list(g.successors(node)):
          node=t
          #print("hatte schon",t)
      else:
          g.add_node(t), g.add_edge(node, t)
          #print("füge", t," hinzu")
          node=t
      
   print("Nodes:",g.number_of_nodes(),"edges:",g.number_of_nodes())

hashlistfile.close()

#with open('graph.json','wt') as f:
#    data=json_graph.node_link_data(g);
#    f.write(json.dumps(data, sort_keys=True, indent=4))

print("Schreibe Graph nach graph.gml")
print("Nodes:",g.number_of_nodes(),"edges:",g.number_of_nodes())
nx.readwrite.gml.write_gml(g, "graph.gml")


#layout=nx.spring_layout(g, k=1.1/math.sqrt(g.number_of_nodes()), iterations=100)
#plt.figure(figsize=(20,10))
#nx.draw(g, with_labels=True, node_size=60, font_size=20); 
#plt.savefig("graph.png")
#plt.figure(figsize=None)
#plt.clf()

