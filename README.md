# dopv

Debian Openssl PRNG Vulnerability
(Testing RSA 2048-Keys)

(Merker, TODO: erweiterte blacklist mit einbauen, bspw. 
[openssl-blacklist](https://packages.debian.org/jessie/openssl-blacklist)
und 
[openssl-blacklist-extra](https://packages.debian.org/jessie/openssl-blacklist-extra)
)

## Naiver Ansatz
Alle 32768 (=2^15) "schwachen" Schlüssel durchlaufen und jeweils vergleichen.

Als erstes Argument wird eine Textdatei erwartet in der der RSA-Modulus als
Hexadezimalzahl steht, vgl. auch die Skripte x509der-to-pubkey-modulus.py
und csrder-to-pubkey-modulus.py.


    $ cat m
    0xc13c1b36eb4024ab2cd6f62dc592ac05d5b138a3739eb8d6824d0db96664fc012989b73f4314a3e6cad68fbe2e0eb16084dd7a5acdc3527c5de268f958683d7c145439d5a736b95e767b5903bbad7a6ff43cfd8c248f022029415df8c6033a4aa1dd06e58a39bdd0951b3fe6684120a63bb4c560cba28e2f419d76c85fb15b6bb0d531ce2221da492b21f49b339820221513f28ad63a87db6d3550ca968021b95a521c99461dc8daf08e68119bdb28cf2e4f5ddc1e40c7259fdd6c1df021df70d99113066db5c093dc725b6255ffc248d37390d230911f826884ea95640dada930e8c70d1060649a74714b0e1dd280c166e7105e10c345d00587cfbd26057d2d

    $ time ./dopv-test-via-hashlist.py m
    Schwacher Schlüssel (Debian Openssl PRNG Vulnerability).

    real    0m0,063s
    user    0m0,062s
    sys     0m0,000s

    $ echo $?
    1

    $ # negativ-test
    $ echo "0x1234567890" >m
    $ ./dopv-test-via-hashlist.py m
    $ echo $?
    0

    $ # Sanity-Check
    $ cat moduluslistfile.txt | while read a; do echo $a>/tmp/m; ./dopv-test-via-hashlist.py /tmp/m; done | wc -l
    32768


## Bloomfilter nach "Method 1" mit false-positives (fp-rate=2^-49)

    $ time ./dopv-test-via-bloomfilter.py m
    Schwacher Schlüssel (Debian Openssl PRNG Vulnerability).

    real    0m0,053s
    user    0m0,050s
    sys     0m0,003s

    $ # negativ-test
    $ echo "0x1234567890" >m
    $ ./dopv-test-via-bloomfilter.py m
    $ echo $?
    0

    $ # Sanity-Check
    $ cat moduluslistfile.txt | while read a; do echo $a>/tmp/m ; ./dopv-test-via-bloomfilter.py /tmp/m; done | wc -l
    32768
    
### In C programmiert
Die python-Implementierung hat den Fokus auf leichte Lesbarkeit bzw. Verständlichkeit, nicht Ausführungsgeschwindigkeit.
Falls die Ausführungsgeschwindigkeit ein Problem sein sollte, sollte man eine Implementierung in C wählen.

Die C-Implementierung liegt in "c_dopv-test-via-bloomfilter.c".
Zunächst muss man die xxhash-Biblithek installiert haben (https://github.com/Cyan4973/xxHash ).
Dann das Programm kompilieren:

    $ make
    gcc -O3 -o c_dopv-test-via-bloomfilter c_dopv-test-via-bloomfilter.c -lxxhash -L/usr/local/lib
    
Eine Millionen Testschlüssel erzeugen.

    $ make c_testschluessel.bin
    dd if=/dev/urandom bs=256 of=c_testschluessel.bin count=1000000
    1000000+0 Datensätze ein
    1000000+0 Datensätze aus
    256000000 bytes (256 MB, 244 MiB) copied, 5,43155 s, 47,1 MB/s
    $ ls -l c_testschluessel.bin 
    -rw-r--r-- 1 a users 256000000 24. Okt 10:03 c_testschluessel.bin

Starten:

    $ ./c_dopv-test-via-bloomfilter 
    Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
    Größe Bloomfilter: 2476064, Anzahl Bit-Zellen: 77377
    Los geht\'s
    Fertig
    Benötigte Zeit: 0.105295000 Sekunden, Zeit (Sekunden) pro Element: 0.000000105
    bye

Der Test benötigt damit deutlich weniger als 1 Mikrosekunde pro Schlüssel.

Im python als "batch-job" für 10^6 Schlüssel

    $ time ./dopv-test-via-bloomfilter_batch2.py 
    Ich untersuche 1000000 RSA-Moduli
    Los geht\'s
    Fertig
    Benötigte Zeit: 4.4245405197143555 Sekunden, Zeit (Sekunden) pro Element: 4.424540519714355e-06

    real    0m4,471s
    user    0m4,340s
    sys     0m0,127s

Mit 4,4 Mikrosekunden pro Schlüssel auch nicht so schlecht. Die Startup-Kosten
des Python-Interpreters werden mit größerer Anzahl der RSA-Moduli prozentual
in Bezug auf die Gesamtausführszeit immer geringer.

Startup-Kosten:

    $ time python -c 'print("hi")'
    hi

    real    0m0,033s
    user    0m0,026s
    sys     0m0,007s


Der Test wurde auf meinem neun Jahre altem Linux-PC ausgeführt.
Als Geschwindigkeitsreferenzwert:

    $ openssl speed rsa2048
    Doing 2048 bits private rsa\'s for 10s: 4280 2048 bits private RSA\'s in 9.99s
    Doing 2048 bits public rsa\'s for 10s: 145376 2048 bits public RSA\'s in 9.99s
    OpenSSL 1.1.1  11 Sep 2018
    built on: Tue Sep 11 19:36:05 2018 UTC
    options:bn(64,64) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr)
    compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall -O3 -Wa,--noexecstack -D_FORTIFY_SOURCE=2 -march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong -fno-plt -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAES_ASM -DVPAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPADLOCK_ASM -DPOLY1305_ASM -DNDEBUG
                      sign    verify    sign/s verify/s
    rsa 2048 bits 0.002334s 0.000069s    428.4  14552.2

Merker: https://gist.github.com/jboner/2841832

Mal als Experiment genau einen RSA-Modulus nehmen:

    $ dd if=/dev/urandom of=c_testschluessel.bin bs=256 count=1
    1+0 Datensätze ein
    1+0 Datensätze aus
    256 bytes copied, 0,00262802 s, 97,4 kB/s
    $ ll c_testschluessel.bin
    -rw-r--r-- 1 a users 256 25. Okt 08:37 c_testschluessel.bin

    $ ./c_dopv-test-via-bloomfilter
    Testdateigröße: 256, Anzahl RSA-Moduli: 1
    Größe Bloomfilter: 2476064, Anzahl Bit-Zellen: 77377
    Los geht\'s
    Fertig
    Benötigte Zeit: 0.000005 Sekunden, Zeit (Sekunden) pro Element: 0.000005
    bye

Mal mit vier Millionen RSA-Moduli

    $ dd if=/dev/urandom bs=256 of=c_testschluessel.bin count=4000000
    4000000+0 Datensätze ein
    4000000+0 Datensätze aus
    1024000000 bytes (1,0 GB, 977 MiB) copied, 22,9428 s, 44,6 MB/s
    $ ./c_dopv-test-via-bloomfilter 
    Testdateigröße: 1024000000, Anzahl RSA-Moduli: 4000000
    Größe Bloomfilter: 2476064, Anzahl Bit-Zellen: 77377
    Los geht\'s
    Fertig
    Benötigte Zeit: 0.413729 Sekunden, Zeit (Sekunden) pro Element: 0.000000
    bye

    $ time ./c_dopv-test-via-bloomfilter
    Testdateigröße: 1024000000, Anzahl RSA-Moduli: 4000000
    Größe Bloomfilter: 2476064, Anzahl Bit-Zellen: 77377
    Los geht\'s
    Fertig
    Benötigte Zeit: 0.415150 Sekunden, Zeit (Sekunden) pro Element: 0.000000
    bye

    real    0m0,496s
    user    0m0,377s
    sys     0m0,117s

Aus Spass fügen wir mal bei den vier Millionen RSA-Moduli einen DOPV-anfälligen
hinzu

    $ head -1 moduluslistfile.txt >one_weak_key.txt
    $ ./txt-to-bin.py one_weak_key.txt >/dev/null
    $ time ./c_dopv-test-via-bloomfilter
    Testdateigröße: 1024000256, Anzahl RSA-Moduli: 4000001
    Größe Bloomfilter: 2476064, Anzahl Bit-Zellen: 77377
    Los geht\'s
    Gefunden! Schwacher Schlüssel bei Index-Nummer: 4000000
    Fertig
    Benötigte Zeit: 0.492387000 Sekunden, Zeit (Sekunden) pro Element: 0.000000123
    bye

    real    0m0,607s
    user    0m0,414s
    sys     0m0,113s

    $ time ./c_dopv-test-via-bloomfilter
    Testdateigröße: 1024000256, Anzahl RSA-Moduli: 4000001
    Größe Bloomfilter: 2476064, Anzahl Bit-Zellen: 77377
    Los geht\'s
    Gefunden! Schwacher Schlüssel bei Index-Nummer: 4000000
    Fertig
    Benötigte Zeit: 0.490207000 Sekunden, Zeit (Sekunden) pro Element: 0.000000123
    bye

    real    0m0,580s
    user    0m0,437s
    sys     0m0,135s

Index-Nummer 4\*10^6 bedeutet der 4\*10^6+1-ste Schlüssel, der erste Schlüssel im
Datensatz hat Index-Nummer 0.

