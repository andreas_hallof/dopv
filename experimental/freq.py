#! /usr/bin/env python3

from collections import defaultdict
from gmpy2 import next_prime
import time, xxhash, hashlib

modulusliste=[]; hashwertliste=[]
start=time.time()
with open("../moduluslistfile.txt", "rt") as f:
    for x in f:
        x=x.rstrip()
        n=int(x,16)
        d=n.to_bytes(256,byteorder = 'big')
        modulusliste.append(d)

        if 1==1:
            hashwertliste.append(xxhash.xxh64(d).intdigest())
        else:
            h=hashlib.sha256(d).digest()
            h=h[-4:]
            hashwertliste.append(int.from_bytes(h, byteorder='big', signed=False))

print("Zeit für das Einlesen", time.time()-start)

p = 2**16+1

# 4 GiB ;-)
newminsize=2**32

log=open("log.txt","wt")

counter=0
while counter<10**5:
    
    counter+=1
    d = defaultdict(int)
    d[0]=0
    p = next_prime(p)
    for x in hashwertliste:
        d[x%p]+=1
    freq = defaultdict(int)
    for x in d:
        freq[d[x]]+=1

    #if not 5 in freq.keys():
    #    print(counter, p, "max:", max(freq.keys()), freq)

    mysize=max(freq.keys())*10*p
    #print(p, mysize)
    log.write(str(p)+" "+str(mysize)+"\n")
    if mysize<newminsize:
        newminsize=mysize
        print("\nNeues Minimum bei p={}, mit {} B ({} KiB) und max-keys={}\n".format(
                p, newminsize, newminsize/1024.0, max(freq.keys()))
             )

log.close()

# optimum 77377 mit xxhash64 3095080 B (3022.539062 KiB) und max-keys=4
# optimum 78511 mit sha256   3140440 B (3066.835938 KiB) und max-keys=4


