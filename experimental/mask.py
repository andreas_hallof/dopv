#! /usr/bin/env python3

import sys, hashlib, os, xxhash, binascii, time

mask=bytearray(32) #print(mask)

moduluslistfile=open("../hashlistfile.txt","rt");
counter=0

for x in moduluslistfile:
   x=x.rstrip()
   counter+=1;
   b=binascii.unhexlify(x)
   for i in range(0,32):
        mask[i]=mask[i] | b[i]

   one_counter=0
   for i in mask:
       if i==255:
           one_counter+=8
           continue

       for t in range(0,8):
           if 2**t & i == 2**t:
               #print("inc", 2**t, i)
               one_counter+=1

   #print(one_counter, binascii.hexlify(mask))
   print("{0:4d} {1:3d} {2:.5f}".format(
                               counter,
                               one_counter,
                               float(one_counter)/float(32*8)
                        ),
                        binascii.hexlify(mask)
   )

   time.sleep(1)

moduluslistfile.close()

